import React from 'react';
import  {AppProvider, Button}  from '@shopify/polaris';
import enTranslations from '@shopify/polaris/locales/en.json';
import "@shopify/polaris/build/esm/styles.css";

function SuperButton (props) {
 console.log("INCOMING")
  return(
 <div>
  <AppProvider i18n={enTranslations}>
    <Button {...props}/>
  </AppProvider>
 </div>
  )
}

export default SuperButton;